﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proizvod
{
    public class Proizvod
    {
        //Napraviti objekte na nacin prikazan u glavnom programu.
        // DAO je skracenica od Data Acess Object i pise se uvek ovako.
        //Samo niz property-a koji ce sluziti kao model.

        public int ProizvodId { get; set; }
        public string NazivProizvoda { get; set; }
        public string Kategorija { get; set; }
        public int JedinicnaCena { get; set; }
        public int JedinicaNaLageru { get; set; }
        private List<Proizvod> listaProizvoda;

        public Proizvod()
        {
            
        }

        //Napraviti funkciju koja ce da vraca listu objekata.
        public List<Proizvod> GetListaProizvoda()
        {
            this.listaProizvoda = new List<Proizvod>
                {
                    new Proizvod
                    {
                        ProizvodId = 1,
                        NazivProizvoda = "Cips",
                        Kategorija = "Grickalice",
                        JedinicnaCena = 50,
                        JedinicaNaLageru = 15
                    },
                new Proizvod
                {
                ProizvodId = 2,
                NazivProizvoda = "Smoki",
                Kategorija = "Grickalice",
                JedinicnaCena = 30,
                JedinicaNaLageru = 20
                },
                new Proizvod
                {
                ProizvodId = 3,
                NazivProizvoda = "Grisini",
                Kategorija = "Grickalice",
                JedinicnaCena = 45,
                JedinicaNaLageru = 12
                }
            };

            return this.listaProizvoda;
        }



       


    }
}
