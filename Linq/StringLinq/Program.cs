﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringLinq
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] imena = { "Aca","Pera","Mika","Lazar","Ivana,",
            "Jovan", "Jovana", "Djordje"};

            var upit = from ime in imena
                       where ime.Length == 5
                       select ime;

            IEnumerable<string> upiNiz = imena.Where(x => x.Length == 5).Select(x => x);

            var upitNiz = imena.Where(x => x.Length == 5);


            foreach (string clan in upit)
            {
                Console.WriteLine(clan + "\n");
            }



        }
    }
}
